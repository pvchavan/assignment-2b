**package to generate csv file**

# HBaseReader.java

Parses data from existing HBase table into List of Objects

Writes the data into a csv file and save it on HDFS

# Person.java
Bean class to save data from HBase

---

**package to generate hfile and populate data to new table**

# HBaseBulkLoadDriver.java
Driver class of MapReduce job to create hfile from CSV.

Calls HBaseBulkLoad method after successful execution of job

# HBaseBulkLoadMapper.java
Mapper class for creating hfile

map function - return Put object for new table and column family

fetched values from CSV (Text input)


# HBaseBulkLoad.java
Populate table from hfile created by the mapreduce job.

input - path of HFile


