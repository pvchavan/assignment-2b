package com.assignment2b.csvgen.bean;

public class Person {
    private String rowNum;
    private String name;
    private String age;
    private String company;
    private String buildingCode;
    private String phoneNumber;
    private String address;

    public Person(String rowNum, String name, String age, String company,
                  String buildingCode, String phoneNumber, String address) {
        this.rowNum = rowNum;
        this.name = name;
        this.age = age;
        this.company = company;
        this.buildingCode = buildingCode;
        this.phoneNumber = phoneNumber;
        this.address = address;
    }

    public String getRowNum() {
        return rowNum;
    }

    public void setRowNum(String rowNum) {
        this.rowNum = rowNum;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getBuildingCode() {
        return buildingCode;
    }

    public void setBuildingCode(String buildingCode) {
        this.buildingCode = buildingCode;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age='" + age + '\'' +
                ", company='" + company + '\'' +
                ", buildingCode='" + buildingCode + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", address='" + address + '\'' +
                '}';
    }

    public String[] toArray() {
        return new String[]{rowNum, name, age, company, buildingCode, phoneNumber, address};
    }

    public String toTxt() {
        return rowNum + ", " + name + ", " + age + ", " + company + ", " +
                buildingCode + ", " + phoneNumber + ", " + address + "\n";
    }
}
