package com.assignment2b.csvgen;

import com.assignment2b.csvgen.bean.Person;
import com.opencsv.CSVWriter;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;
import java.util.List;

public class HBaseReader {
    public static String EXISTING_TABLE = "People_Info";
    public static String URI = "hdfs://localhost:9000";
    public static String HDFS_DIR = "hdfs://localhost:9000/capstone/People_Info.csv";

    public static void main(String[] args) {
        HBaseReader hBaseReader = new HBaseReader();
        List<Person> personList = hBaseReader.parseHBaseData();
        hBaseReader.createCSV(personList);
    }

    public List<Person> parseHBaseData() {
        List<Person> people = new LinkedList<>();
        try {
            Configuration conf = HBaseConfiguration.create();
            Connection connection = ConnectionFactory.createConnection(conf);
            Table hTable = connection.getTable(TableName.valueOf(EXISTING_TABLE));
            Scan scan = new Scan();
            ResultScanner scanner = hTable.getScanner(scan);
            Result r;
            while (((r = scanner.next()) != null)) {
                byte[] key = r.getRow();
                byte[] col_family = Bytes.toBytes("Name");
                byte[] name = r.getValue(col_family, Bytes.toBytes("Name"));
                byte[] age = r.getValue(col_family, Bytes.toBytes("Age"));
                byte[] company = r.getValue(col_family, Bytes.toBytes("Company"));
                byte[] buildingCode = r.getValue(col_family, Bytes.toBytes("Building_code"));
                byte[] phoneNumber = r.getValue(col_family, Bytes.toBytes("Phone_Number"));
                byte[] address = r.getValue(col_family, Bytes.toBytes("Address"));

                people.add(new Person(Bytes.toString(key), Bytes.toString(name), Bytes.toString(age),
                        Bytes.toString(company), Bytes.toString(buildingCode),
                        Bytes.toString(phoneNumber), Bytes.toString(address)));
            }
            scanner.close();
            hTable.close();
        } catch (IOException e) {
            System.out.println("Caught IO Exception");
            e.printStackTrace();
        } catch (Exception e) {
            System.out.println(e + " occurred.");
        }
        return people;
    }

    public void createCSV(List<Person> personList) {

        try {
            Configuration configuration = HBaseConfiguration.create();
            FileSystem hdfs = FileSystem.get(new URI(URI), configuration);
            Path file = new Path(HDFS_DIR);
            if (hdfs.exists(file)) {
                hdfs.delete(file, true);
            }
            OutputStream os = hdfs.create(file,
                    () -> System.out.println("...bytes written: [ ]"));

            CSVWriter csvWriter = new CSVWriter(new OutputStreamWriter(os, StandardCharsets.UTF_8));
            for (Person p : personList) {
                csvWriter.writeNext(p.toArray());
            }
            csvWriter.close();
            hdfs.close();
            os.close();
        }
        catch (IOException e) {
            System.out.println("IO Exception \n" + e);
        }
        catch (Exception e) {
            System.out.println(e + "\n Exception occurred");
        }
    }
}
