package com.assignment2b.hfile;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.HFileOutputFormat2;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.net.URI;

public class HBaseBulkLoadDriver extends Configured implements Tool {

    private static final String DATA_SEPARATOR = ",";
    private static final String TABLE_NAME = "people_bulk_upload";
    private static final String COLUMN_FAMILY = "details";
    private static final String INPUT_PATH = "hdfs://localhost:9000/capstone/People_Info.csv";
    private static final String OUTPUT_PATH = "hdfs://localhost:9000/hfiles/out/";

    public static void main(String[] args) {
        try {
            int response = ToolRunner.run(HBaseConfiguration.create(),
                    new HBaseBulkLoadDriver(), args);
            if (response == 0) {
                System.out.println("Job is successfully completed...");
            } else {
                System.out.println("Job failed...");
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    @Override
    public int run(String[] args) throws Exception {
        int result = 0;
        Configuration configuration = HBaseConfiguration.create();
        configuration.set("data.separator", DATA_SEPARATOR);
        configuration.set("hbase.table.name", TABLE_NAME);
        configuration.set("COLUMN_FAMILY", COLUMN_FAMILY);
        configuration.set("hbase.fs.tmp.dir", "/Users/prathameshchavan/hbase-staging");
        Job job = Job.getInstance(configuration);//new Job(configuration);
        job.setJarByClass(HBaseBulkLoadDriver.class);
        job.setJobName("bulk_upload_hFile");
        job.setInputFormatClass(TextInputFormat.class);
        job.setMapOutputKeyClass(ImmutableBytesWritable.class);
        job.setMapperClass(HBaseBulkLoadMapper.class);
        FileInputFormat.addInputPaths(job, INPUT_PATH);
        FileSystem.get(new URI(OUTPUT_PATH), configuration).delete(new Path(OUTPUT_PATH), true);
        FileOutputFormat.setOutputPath(job, new Path(OUTPUT_PATH));
        job.setMapOutputValueClass(Put.class);
        Connection connection = ConnectionFactory.createConnection(configuration);
        Table hTable = connection.getTable(TableName.valueOf(TABLE_NAME));
        RegionLocator region = connection.getRegionLocator(TableName.valueOf(TABLE_NAME));
        HFileOutputFormat2.configureIncrementalLoad(job, hTable, region);

        job.waitForCompletion(true);
        if (job.isSuccessful()) {
            HBaseBulkLoad.doBulkLoad(OUTPUT_PATH, TABLE_NAME);
        } else {
            result = -1;
        }
        System.out.println("Done");
        return result;
    }
}

