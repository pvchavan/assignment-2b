package com.assignment2b.hfile;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class HBaseBulkLoadMapper
        extends Mapper<LongWritable, Text, ImmutableBytesWritable, Put> {
    private String hbaseTable;
    private String dataSeparator;
    private String columnFamily;
    private ImmutableBytesWritable hbaseTableName;

    public void setup(Context context) {
        Configuration configuration = context.getConfiguration();
        hbaseTable = configuration.get("hbase.table.name");
        dataSeparator = configuration.get("data.separator");
        columnFamily = configuration.get("COLUMN_FAMILY");
        hbaseTableName = new ImmutableBytesWritable(Bytes.toBytes(hbaseTable));
    }

    public void map(LongWritable key, Text value, Context context) {
        try {
            String[] values = value.toString().split(dataSeparator);
            String rowKey = values[0].replaceAll("\"", "");
            Put put = new Put(Bytes.toBytes(rowKey));

            put.addColumn(Bytes.toBytes(columnFamily), Bytes.toBytes("name"),
                    Bytes.toBytes(values[1].replaceAll("\"", "")));
            put.addColumn(Bytes.toBytes(columnFamily), Bytes.toBytes("age"),
                    Bytes.toBytes(values[2].replaceAll("\"", "")));
            put.addColumn(Bytes.toBytes(columnFamily), Bytes.toBytes("company"),
                    Bytes.toBytes(values[3].replaceAll("\"", "")));
            put.addColumn(Bytes.toBytes(columnFamily), Bytes.toBytes("building_code"),
                    Bytes.toBytes(values[4].replaceAll("\"", "")));
            put.addColumn(Bytes.toBytes(columnFamily), Bytes.toBytes("phone_number"),
                    Bytes.toBytes(values[5].replaceAll("\"", "")));
            put.addColumn(Bytes.toBytes(columnFamily), Bytes.toBytes("address"),
                    Bytes.toBytes(values[6].replaceAll("\"", "")));
            context.write(hbaseTableName, put);
        } catch(Exception exception) {
            exception.printStackTrace();
        }
    }
}
